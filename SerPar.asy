Version 4
SymbolType BLOCK
RECTANGLE Normal -64 -136 80 136
WINDOW 0 8 -136 Bottom 2
PIN -64 112 LEFT 8
PINATTR PinName CLK
PINATTR SpiceOrder 1
PIN -64 0 LEFT 8
PINATTR PinName IN
PINATTR SpiceOrder 2
PIN -64 -112 LEFT 8
PINATTR PinName VDD
PINATTR SpiceOrder 3
PIN 80 -112 RIGHT 8
PINATTR PinName OUT0
PINATTR SpiceOrder 4
PIN 80 -80 RIGHT 8
PINATTR PinName OUT1
PINATTR SpiceOrder 5
PIN 80 -48 RIGHT 8
PINATTR PinName OUT2
PINATTR SpiceOrder 6
PIN 80 -16 RIGHT 8
PINATTR PinName OUT3
PINATTR SpiceOrder 7
PIN 80 16 RIGHT 8
PINATTR PinName OUT4
PINATTR SpiceOrder 8
PIN 80 48 RIGHT 8
PINATTR PinName OUT5
PINATTR SpiceOrder 9
PIN 80 80 RIGHT 8
PINATTR PinName OUT6
PINATTR SpiceOrder 10
PIN 80 112 RIGHT 8
PINATTR PinName OUT7
PINATTR SpiceOrder 11
